class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.date = document.getElementById("tanggal");
    this.time = document.getElementById("waktu");
    this.penumpang = document.getElementById("penumpang");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = async () => {
    await this.load();
    this.clear();

    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      // this.carContainerElement.appendChild(node);

      const col = document.createElement("div");
      col.className = "col-lg-4 col-md-12";
      col.innerHTML = car.render();
      this.carContainerElement.appendChild(col);
    });
  };

  async load() {
    const dateValue = this.date.value;
    const timeValue = this.time.value;
    const penumpangValue = this.penumpang.value;
    
    console.log("Date",dateValue);
    console.log("Time",timeValue);
    console.log("jmlhPenumpang",penumpangValue);
    
    const dateTime = new Date (`${dateValue} ${timeValue}`);
    console.log("Date Time:",dateTime);

    const epochTime = dateTime.getTime();
    console.log("epochTime:",epochTime);

    const cars = await Binar.listCars((item) =>{
      const showPenumpang = item.capacity >= penumpangValue;
      const showDateTime = item.availableAt.getTime() < epochTime;
      return showPenumpang && showDateTime;
    });
    Car.init(cars);
  }


  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
