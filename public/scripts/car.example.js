class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="card mb-4">
        <div class="card-body">
          <img src="${this.image}" class="img-fluid" alt="${this.manufacture}" style="height:400px; object-fit:cover;">
          <h6 class="mt-2">${this.manufacture} / ${this.model}</h6>
          <h5><b>Rp.${this.rentPerDay} / hari</b></h5>
          <p class="card-text" style="font-size:14.7px; height:75px;">${this.description}</p>
          <p style="font-size:14.7px;"><img src="./img/fi_users.png">  ${this.capacity} Orang</p>
          <p style="font-size:14.7px;"><img src="./img/fi_settings.png">  ${this.transmission}</p>
          <p style="font-size:14.7px;"><img src="./img/fi_calendar.png">  Tahun ${this.year}</p>
          <p style="font-size:14.7px;">Tersedia:<b>${this.availableAt}</b></p>
        </div>
        <input class="btn btn-success btn-sm mx-3 my-2" type="submit" value="Pilih Mobil">
      </div>
    `;
  }
}
